﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
//for locking
using System.Runtime.InteropServices;
//for debug log
using System.Diagnostics;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.Devices.Bluetooth.Advertisement;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //for locking the computer
        //http://stackoverflow.com/questions/13745788/how-do-i-lock-a-windows-workstation-programmatically
        [DllImport("user32.dll")]
        public static extern bool LockWorkStation();

        private BluetoothLEAdvertisementWatcher bluetoothLEAdvertisementWatcher;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void OnAdvertisementReceived(BluetoothLEAdvertisementWatcher watcher, BluetoothLEAdvertisementReceivedEventArgs eventArgs)
        {
            Debug.WriteLine("BT received");
            //display adress for debug
            Debug.WriteLine(eventArgs.BluetoothAddress); 
            var manufacturerSections = eventArgs.Advertisement.ManufacturerData;
            if (manufacturerSections.Count > 0)
            {
                var manufacturerData = manufacturerSections[0];
                var data = new byte[manufacturerData.Data.Length];
                using (var reader = Windows.Storage.Streams.DataReader.FromBuffer(manufacturerData.Data))
                {
                    reader.ReadBytes(data);

                    // If we arrive here we have detected a Bluetooth LE advertisement
                    // Add code here to decode the the bytes in data and read the beacon identifiers
                    Debug.WriteLine("My 333 error message.");
                }
            }
        }

        public void LookForBeacons()
        {
            bluetoothLEAdvertisementWatcher = new BluetoothLEAdvertisementWatcher();
            bluetoothLEAdvertisementWatcher.Received += OnAdvertisementReceived;
            bluetoothLEAdvertisementWatcher.Start();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            
            LookForBeacons();

            Debug.WriteLine("Looking for beacon");
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (!LockWorkStation())
            {
                Debug.WriteLine(Marshal.GetLastWin32Error());
                throw new System.ComponentModel.Win32Exception(Marshal.GetLastWin32Error());
            }
        }
    }
}
